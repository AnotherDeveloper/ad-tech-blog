package org.another_developer.web;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Jasper C. (Another Developer) <mail@another-developer.org>
 */

//TODO: Check all try-catches, if, and switch statements for propper error handling and logging

public class Client_Interaction extends HttpServlet {

    HttpServletRequest request;
    HttpServletResponse response;
    
    private static final Logger logger = Logger.getLogger(Client_Interaction.class.getName());
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String URI = request.getRequestURI();
            
            switch (URI) {
                case "":
                case "/":
                    response.sendRedirect("./df");
                    break;
                case "/df":
                    response.setContentType("text/html");
                    getFile(response, "/html/", URI + ".html");
                    break;
                default:
                    if (URI.startsWith("/blobs")) {
                        //Hadle Cloud Storage requests
                        
                        break;
                    }

                    response.sendError(404, "File Not Found");
                    
                    if (request.getHeader("Accept").contains("text/html")) response.sendRedirect("./df?p=error:404");
            }
        } catch (IOException | NullPointerException ex) {
            response.sendError(500, "\nSomething went horribly wrong!!! DX )%*&$#*(W *table flip* *world endedsJ(#WHHIWLIFEDOESNTEXIST*");
            logger.log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        this.request = request;
        this.response = response;
        
        try {
            //Starts reading the JSON data sent from a client
            String inputLine;
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader bufferedReader;
            bufferedReader = new BufferedReader(new InputStreamReader(request.getInputStream()));
            while ((inputLine = bufferedReader.readLine()) != null) { stringBuilder.append(inputLine); }
            bufferedReader.close();
            
            //Data is put into a JSONObject
            JSONObject jsonResponse = new JSONObject(stringBuilder.toString());
            
            switch (jsonResponse.getString("type")) {
                case "page": 
                    //If the type identifier is "page", we shall grab HTML and send something back
                    String url[] = jsonResponse.getString("page").split("/");

                    if (url[3].equals("df")) {
                        getFile(response, "/html/", "home.html");
                    } else if (url[3].startsWith("df?") && url[3].contains("p=")) {
                        
                        String[] queryString = url[3].replace("df?", "").split("&");
                        
                        boolean isPage = false;
                        
                        for (int i = 0; i < queryString.length; i++) {
                            if (isPage) break;
                            
                            switch (queryString[i]) {
                                case "p=software":
                                case "p=electrical":
                                case "p=animation":
                                    getFile(response, "/html/", "blog" + ".html");
                                    isPage = true;
                                    break;
                                default:
                                    getFile(response, "/html/", url[3].replace("df?p=", "") + ".html");
                                    break;
                            }
                        }
                    } else {
                        response.setStatus(404);
                    }

                    break;
                case "datastore":
                    response.getOutputStream().print(grabData(jsonResponse));
                    break;
                case "authentication": //Authenticates a reCaptcha confirmation request
                    //A URL connection is opened with the reCaptcha server and headers are specified             ... this secret doesn't seem to be so secret
                    URL recaptcha = new URL("https://www.google.com/recaptcha/api/siteverify?secret=6LdtUwoTAAAAAGnLKBo3iOaU1gtBL_93v6NGH5ar&response=" + jsonResponse.getString("response") + "&remoteip=" + request.getRemoteAddr());
                    HttpURLConnection connection = (HttpURLConnection) recaptcha.openConnection();
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0");

                    //Takes the output JSON response of the reCaptcha server, and then
                    //validating the response, storing the users request, and sending
                    //the reCaptcha response to the client.
                    bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder data = new StringBuilder();

                    while ((inputLine = bufferedReader.readLine()) != null) { data.append(inputLine); }
                    bufferedReader.close();
                    
                    response.getOutputStream().print(data.toString());

                    if (data.toString().contains("\"success\": true")) {
                        jsonResponse.remove("type");
                        jsonResponse.remove("response");
                        
                        DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
                        Entity entity = new Entity("contact");
                        
                        entity.setProperty("name", jsonResponse.getString("name"));
                        entity.setProperty("email", jsonResponse.getString("email"));
                        entity.setProperty("ip", this.request.getRemoteAddr());
                        entity.setProperty("message", new Text(jsonResponse.getString("message")));

                        datastoreService.put(entity);
                    }

                    break;
                default:
                    //A silly response to people who're messing around with POST requests to this server
                    response.sendError(666, "cat.exe has stopped working.");
                    break;
            }
        } catch (Exception ex) {
            logger.log(Level.WARNING, null, ex);
        } 
    }
    
    /**
     * This function grabs the file that is being requested. If it doesn't exist or something goes wrong, an error is returned to the client.
     * 
     * @param path The path to the package that contains the file
     * @param file The full file name
     * @return Data in the form of a String contained with-in the file
     */
    private void getFile(HttpServletResponse response, String path, String file) {
        try {
            String inputLine;
            StringBuilder stringBuilder = new StringBuilder();
            
            BufferedReader bufferedReader;
            bufferedReader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(path + file)));

            while ((inputLine = bufferedReader.readLine()) != null) { stringBuilder.append(inputLine).append("\n"); }
            bufferedReader.close();

            response.getOutputStream().print(stringBuilder.toString());
        } catch(IOException | NullPointerException ex) {
            response.setStatus(404);
        } catch(Exception ex) {
            response.setStatus(500);
            logger.log(Level.WARNING, null, ex);
        }
    }
    
    /**
     * Idk what this function does yet.
     * 
     * @param jsonRequest 
     * @return Returns the requested data
     */
    private String grabData(JSONObject jsonRequest) throws JSONException {
        JSONArray jsonResponse  = new JSONArray();
        DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
        Query query = new Query(jsonRequest.getString("entity")).addSort("date", SortDirection.DESCENDING);
        PreparedQuery preQuery = datastoreService.prepare(query);
        
        // TODO: Grab entries using an offset+limit setup.
        // Doesn't seem to be possible with the current classes that are defined.
        // Re-enable sending index in retrieve.js
        
        switch (jsonRequest.getString("mode")) {
            case "pull":
                JSONArray keys = jsonRequest.getJSONArray("key");
                
                for (Entity result : preQuery.asIterable()) {
                    //Simply looping ... I can do this more efficiently ya know
                    
                    for (int i = 0; i < keys.length(); i++) {
                        if (KeyFactory.keyToString(result.getKey()).equals(keys.get(i))) {
                            JSONObject entry = new JSONObject();
                            entry.put("date", result.getProperty("date"));
                            Text data = (Text) result.getProperty("data");
                            entry.put("data", new JSONObject(data.getValue()));
                            
                            jsonResponse.put(entry);
                        }
                    }
                }
                
                return jsonResponse.toString();
            case "list":
                for (Entity result : preQuery.asIterable()) {
                    JSONObject entry = new JSONObject();
                    entry.put("key", KeyFactory.keyToString(result.getKey()));
                    entry.put("title", (String) result.getProperty("title"));
                    
                    jsonResponse.put(entry);

                    if (jsonResponse.length() == 345) break;
                }

                return jsonResponse.toString();
            //case "lenght":
            //    return Integer.toString(preQuery.countEntities(FetchOptions.Builder.withLimit(345)));
        }
        
        return null;
    }
}