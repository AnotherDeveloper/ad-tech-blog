var pager = '<div id="pager" class="row"><div class="col-lg-12"><ul class="pager"><li class="previous"><a class="previous-btn" href="javascript:previousPage()">&larr; Older</a></li><li class="next"><a class="next-btn" href="javascript:nextPage()">Newer &rarr;</a></li></ul></div></div><!-- /.row -->';

var list, entity;

var queryString = document.location.href.split("?")[1];
var queryParameters = queryString.split("&");

// Sets variable entity based on the page
for (var p in queryParameters) {
    if (queryParameters[p].startsWith("p=")) {
        switch(queryParameters[p]) {
            case "p=electrical": entity = "electEng"; document.title = 'Hardware Design - Another Developer'; break;
            case "p=software": entity = "softDev"; document.title = 'Software Development - Another Developer'; break;
            case "p=animation": entity = "animation"; document.title = 'Animation - Another Developer'; break;
        }
    }
}

// Request data that's to be sent to the server
var json = {};
json.type = "datastore";
json.mode = "list";
json.entity = entity;

// Makes a post request to the server 
// The server returns an array of JSON data enties, which are then stored in the variable list
// If the list is empty, HTML is added to the page to notify the user that there's nothing to showcase
$.post('/', JSON.stringify(json))
    .done((data) => {
        list = $.parseJSON(data);
        
        if (list.length > 0) {
            grabContent();
        } else {
            document.getElementById("content").innerHTML += entryBuilder('[]');
        }
    })
    .fail((error) => {
        console.log(error.status);
    });

// Sets the height of all elements with the post-media class, so that it's in a 
// 16:9 aspect ratio with the width. And adjusts the scroll position of each of 
// the elements selected index.
window.addEventListener('resize', () => { //Should be applied to content div, but it doesn't work  // FIXME: Stays behind as a residule listener
    var _postMedia = document.getElementsByClassName("post-media");

    for (var i = 0; i < _postMedia.length; i++) {
        _postMedia[i].style.height = ((_postMedia[i].offsetWidth /16) * 9) + "px";
        _postMedia[i].scrollTo(0, (_postMedia[i].style.height.replace('px', '') * _postMedia[i].getAttribute('index')));
    }
});

/**
 * Grabs the next set of 5 entries and sets the page number in the address bar
 */
function nextPage() {
    if (document.location.href.includes('i=')) {
        for (var i = 0; i <= queryParameters.length; i++) {
            if (queryParameters[i].startsWith('i=')) {
                var index = queryParameters[i].replace( /^\D+/g, '');
                index++;
                
                document.location.href = 
                        document.location.href.replace(queryParameters[i], 'i=' + index);
            }
        }
    } else {
        document.location.href += '&i=1';
    }
}

/**
 * Grabs the previous 5 sets of entries and sets the page number in the address bar
 */
function previousPage() {
    if (document.location.href.includes('i=')) {
        for (var i = 0; i <= queryParameters.length; i++) {
            if (queryParameters[i].startsWith('i=')) {
                var index = queryParameters[i].replace( /^\D+/g, '');
                index--;

                document.location.href = 
                        document.location.href.replace(queryParameters[i], 'i=' + index);
            }
        }
    } else {
        document.location.href += '&i=1';
    }
}

/*
 * Validates to ensure the post is valid and complete and processes the the supplied JSON data into HMTL.
 * 
 * @param {type} data An JSON array of posts to be processed into HTML
 * @param {type} pages Page number the user is on
 * @returns {String} Processed post data
 */
function entryBuilder(data, pages) {
    var postData = [];
    var postEntry = {};
    var postHTML = '';
    
    var emptyDS = {};
    emptyDS.title = 'Empty Datastore';
    emptyDS.media = ['/img/meme/shrug.png'];
    emptyDS.doc = 'I have absolutely nothing to show case for you lovely people! <b>:D</b><br><br>Even though I\'m a crafty person with many different projects. I might just have been a tad bit to lazy to document any of them yet, or the datastore somehow got wiped empty.';

    var procError = {};
    procError.title = 'Processing Error';
    procError.media = ['/img/meme/shrug.png'];
    procError.doc = 'So ... there\'s a problem prevent post from getting to your sparkly and absorbent eyes <b>o_o</b>';
    
    try {
        if (data.startsWith('{')) throw new Error('function entryBuilder() only accepts Arrays');
        
        postData = JSON.parse(data);
        
        if (postData.length === 0) postData = [emptyDS];
    } catch (ex) {
        console.error(ex);
        
        postData = [procError];
    }
    
    for (var i = 0; i < postData.length; i++) {
        try {
            if (typeof postData[i].data !== 'undefined') postEntry = postData[i].data;
            else postEntry = postData[i];

            if (postEntry > 5) {
                console.error('Reached maximum allowed post entries');
                break;
            }
            
            if (typeof postEntry.title !== 'undefined') {
                if (typeof postEntry.media !== 'undefined' || typeof postEntry.doc !== 'undefined' || typeof postEntry.data !== 'undefined') {
                    postHTML += '<div class="row"><div class="col-md-12"><h2 class="page-header">' + postEntry.title + '</h2></div></div>';

                    // If I ever decide to use a template for column width, etc. ... remember to use LET instead of VAR
                    var rowDiv = document.createElement('div');
                        rowDiv.classList = 'row';

                    if (typeof postEntry.media !== 'undefined') {
                        var mediaDiv = document.createElement('div');    
                            mediaDiv.classList.add('col-md-12');
                            mediaDiv.style.marginBottom = '15px';
                        var mediaElementDiv = document.createElement('div');
                            mediaElementDiv.classList.add('post-media');
                            mediaElementDiv.setAttribute('index', '0');
                            mediaElementDiv.style.height = (((document.getElementById("content").offsetWidth - 20) / 16) * 9) + "px"; 
                            // It first takes into account the difference in width between the content div and the to be post-media divs
                            // Then it sets the height to a 16:9 ratio with the just calculated width
                            
                        for (var j in postEntry.media) {
                            var filePath = postEntry.media[j];
                            
                            if (postEntry.media[j].endsWith(".png")) {
                                mediaElementDiv.innerHTML += ('<img class="media-element" src="' + postEntry.media[j] + '"></img>');
                            }
                            
                            if (postEntry.media[j].includes("youtube.com/")) {
                                mediaElementDiv.innerHTML += ('<iframe class="media-element" src="' + postEntry.media[j] + '?rel=0" frameborder="0" allowfullscreen></iframe>');
                            }
                            
                            if (postEntry.media[j].endsWith(".js")) {
                                var fileLocation = postEntry.media[j].split("/");
                                var folderName = fileLocation[fileLocation.length -2];
                                var fileName = fileLocation[fileLocation.length -1];
                                
                                mediaElementDiv.innerHTML += ('<div class="media-element" id="' + folderName + fileName.split(".")[0] + '"></div>');
                                
                                $.getScript(filePath).fail((jQxhr, errorMessage, errorThrown) => {
                                    // console.error(jqxhr);
                                    //
                                    // Look at returned results for:
                                    //   - Timout
                                    //   - File Not Found
                                    //   - Not Authorized
                                    //   - Internal server error
                                    
                                    // What if the script is loaded, but fails in its preInit stage?
                                    // Should be using Detector.js before even loading scene scripts
                                });
                                
                                //https://api.jquery.com/jquery.getscript/
                                //https://stackoverflow.com/questions/14521108/dynamically-load-js-inside-js
                                
                                //TODO: 
                                // When a posting is submitted:
                                //   - File name is retained
                                //   - All files are put in a folder named to the unix time of submission
                                //   - postID var is adjusted to the unix time of submission and filename
                                //   - 
                                //
                                // Now the file refrences are being parsed, the file name is used in an id
                                // attribute for a canvas element. Which the script can use to get its 
                                // designated element in the DOM
                                //
                                // Each 3D scene must show its usable controls before loading the scene
                            }
                        }
                        
                        if (mediaElementDiv.childElementCount > 1) {
                            if (userAgent.includes('android') || userAgent.includes('iphone')) {
                                mediaDiv.innerHTML += '<div class="gallery-controls"><p class="gallery-index">## / ##</p></div>';
                            } else {
                                mediaDiv.innerHTML += '<div class="gallery-controls"><div class="control-left"></div><p class="gallery-index">## / ##</p><div class="control-right"></div></div>';
                            }
                        }
                        
                        mediaDiv.appendChild(mediaElementDiv);
                        rowDiv.appendChild(mediaDiv);
                    }

                    if (typeof postEntry.doc !== 'undefined') {
                        var docDiv = document.createElement('div');    
                        
                        if (typeof postEntry.data !== 'undefined' && postEntry.data.length > 0) {
                            // TODO: Get the inner contents of the GitHub or Google Drive link
                            //       GitHub API on the admin posting thingy
                            var dataDiv = document.createElement('div');
                                dataDiv.classList.add('col-md-12');
                                dataDiv.classList.add('data-div');

                            for (var j in postEntry.data) {
                                var dataVendor = postEntry.data[j].split('@')[0];
                                var dataParameters = JSON.parse('{"' + postEntry.data[j].split('@')[1].replace(/=/g, '":"').replace(/&/g, '","') + '"}');

                                switch(dataVendor) {
                                    case 'github':
                                        // Repo contents:       https://developer.github.com/v3/repos/contents/#get-contents
                                        // 
                                        // Repo download API:   https://developer.github.com/v3/repos/contents/#get-archive-link
                                        //                                            USER-ID        REPOSITRY           BRANCH
                                        // Repositry download:  https://github.com/Another-Developer/AD-Tech-Blog/archive/master.zip

                                        // github@owner=Another-Developer&repo=AD-Tech-Blog
                                        // Keep GitHub to full downloads ... (Scroll down and look in the Google Cloud Storage case statement)

                                        if (typeof dataParameters.owner !== 'undefined' && typeof dataParameters.owner !== 'undefined') {

                                            if (typeof dataParameters.path === 'undefined') dataParameters.path = '';

                                            //TODO: Use a promise instead of making this synchronous
                                            $.ajax('https://api.github.com/repos/' + dataParameters.owner + '/' + dataParameters.repo + '/contents/' + dataParameters.path, {async: false})
                                                    .done((repoContents) => {
                                                        var repoTableElement = document.createElement('table');

                                                        //TODO: Header row for the repo? Does it look & fit in best if I keep to the same stylinhg as titles?
                                                        var repoHeader = document.createElement('tr');
                                                        var repoLinks = document.createElement('td');
                                                        var repoDownload = document.createElement('td');
                                                        
                                                        repoLinks.innerHTML = '<a href="https://github.com/' + dataParameters.owner + '">' + dataParameters.owner + '</a>' + ' / ' +
                                                                '<a href="https://github.com/' + dataParameters.owner + '/' + dataParameters.repo + '">' + dataParameters.repo + '</a>';
                                                        repoDownload.innerHTML = ''; //TODO: Implement repositry download functionality
                                                        
                                                        repoHeader.appendChild(repoLinks);
                                                        repoHeader.appendChild(repoDownload);
                                                        repoTableElement.appendChild(repoHeader);

                                                        //TODO: Add a class to the repo table element to limit width so that  the postings documentry text can 
                                                        //      wrap around it. 45em

                                                        repoContents.sort(function(a,b) {return (a.type > b.type) ? 1 : ((b.type > a.type) ? -1 : 0);} );

                                                        for (var rC in repoContents) {
                                                            var repoContent = repoContents[rC];
                                                            var repoTableEntry = document.createElement('tr');

                                                            var repoEntry = document.createElement('td');
                                                            var repoEntrySize = document.createElement('td');
                                                            var repoEntryTime = document.createElement('td');
                                                            // Create row > Column: identify item type and select appropriate icon > Column: File/Folder name > Column: Size > Column: Time

                                                            repoEntry.classList.add('repo-icon');

                                                            switch (repoContent.type) {
                                                                case 'dir':
                                                                    repoEntry.classList.add('repo-icon-dir');

                                                                    break;
                                                                case 'file':
                                                                    //FIXME: I'm pretty sure there is much more efficient way to set a class per file type
                                                                    switch (repoContent.name.split('.')[1].toLocaleLowerCase()) {
                                                                        case 'avi': repoEntry.classList.add('repo-icon-file-avi'); break;
                                                                        case 'css': repoEntry.classList.add('repo-icon-file-css'); break;
                                                                        case 'csv': repoEntry.classList.add('repo-icon-file-csv'); break;
                                                                        case 'dbf': repoEntry.classList.add('repo-icon-file-dbf'); break;
                                                                        case 'doc': repoEntry.classList.add('repo-icon-file-doc'); break;
                                                                        case 'html': repoEntry.classList.add('repo-icon-file-html'); break;
                                                                        case 'iso': repoEntry.classList.add('repo-icon-file-iso'); break;
                                                                        case 'java': repoEntry.classList.add('repo-icon-file-java'); break;
                                                                        case 'jpg': repoEntry.classList.add('repo-icon-file-jpg'); break;
                                                                        case 'js': repoEntry.classList.add('repo-icon-file-js'); break;
                                                                        case 'json': repoEntry.classList.add('repo-icon-file-json'); break;
                                                                        case 'md': repoEntry.classList.add('repo-icon-file-md'); break;
                                                                        case 'mp3': repoEntry.classList.add('repo-icon-file-mp3'); break;
                                                                        case 'mp4': repoEntry.classList.add('repo-icon-file-mp4'); break;
                                                                        case 'mtl': repoEntry.classList.add('repo-icon-file-mtl'); break;
                                                                        case 'obj': repoEntry.classList.add('repo-icon-file-obj'); break;
                                                                        case 'pdf': repoEntry.classList.add('repo-icon-file-pdf'); break;
                                                                        case 'png': repoEntry.classList.add('repo-icon-file-png'); break;
                                                                        case 'ppt': repoEntry.classList.add('repo-icon-file-ppt'); break;
                                                                        case 'rtf': repoEntry.classList.add('repo-icon-file-rtf'); break;
                                                                        case 'svg': repoEntry.classList.add('repo-icon-file-svg'); break;
                                                                        case 'txt': repoEntry.classList.add('repo-icon-file-txt'); break;
                                                                        case 'xcf': repoEntry.classList.add('repo-icon-file-xcf'); break;
                                                                        case 'xls': repoEntry.classList.add('repo-icon-file-xls'); break;
                                                                        case 'xml': repoEntry.classList.add('repo-icon-file-xml'); break;
                                                                        case 'zip': repoEntry.classList.add('repo-icon-file-zip'); break;
                                                                    }

                                                                    break;
                                                                case 'symlink':
                                                                    repoEntry.classList.add('repo-icon-symlink');

                                                                    break;
                                                                case 'submodule':
                                                                    repoEntry.classList.add('repo-icon-submodule');

                                                                    break;
                                                                default:
                                                                    repoEntry.classList.add('repo-icon-unkown');
                                                                    // Use a blank file icon
                                                                    // TODO: ERROR HANDLING: Throw error, and log (file type)
                                                                    break;
                                                            }

                                                            repoEntry.innerHTML = '<p style="margin: 0;">' + repoContent.name + '</p>';
                                                            repoTableEntry.appendChild(repoEntry);

                                                            if (repoContent.type === 'file') {
                                                                repoEntrySize.innerHTML = '<p style="margin: 0;">' + repoContent.size + '</p>';
                                                                repoTableEntry.appendChild(repoEntrySize);
                                                            } else {
                                                                repoEntrySize.innerHTML = '<br>'; //FIXME: Doesn't feel like the proper way to create an empty cell
                                                                repoTableEntry.appendChild(repoEntrySize);
                                                            }

                                                            repoTableElement.appendChild(repoTableEntry);
                                                        }

                                                        dataDiv.appendChild(repoTableElement);
                                                        docDiv.appendChild(dataDiv);
                                                    })
                                                    .fail((jqXHR, textStatus, errorThrown) => {
                                                        // TODO: ERROR HANDLING: Throw error, log, determine the cause and maybe try again
                                                    });
                                        } else {
                                            // TODO: ERROR HANDLING: Throw an error, log (raw dataParameters string included)
                                        }

                                        break;
                                    case 'gcloud':
                                        // Google Cloud Storage
                                        // ... and allow individual downloads on cloud storage
                                        break;
                                    default:
                                        // TODO: ERROR HANDLING: Throw error, log (include dataVendor)
                                        break;
                                }
                            }
                        }
                        
                        docDiv.classList = 'col-md-12';;
                        docDiv.innerHTML += postEntry.doc;
                        
                        rowDiv.appendChild(docDiv);
                    }

                    postHTML += rowDiv.outerHTML;
                }
            }
        } catch (ex) {
            console.error(ex);
            //console.log('Attempting to fetch post(s) data once more');
            //TODO: Try fetching the data once again
        }
    }
    
    //If there are 5 posts or the query index exists and is 1 or greater, then add pager
    if (postData.length === 5 || pages > 1) {
        postHTML += pager;
    }
    
    return postHTML;
}

/*
 * This first adjusts all post-media gallery index elements so that it shows 
 * (1 / Out of all media elements), and then determines if you're on a phone or 
 * computer, so that it can set the type of controls best suited for the device.
 */
function mediaGalleryControls() {
    //FIXME:
    // ... the controls CSS and JavaScript just feel amaturistic 
    //      (inline styling, scrolling divs (which needs adjusting on window resize), feels a little like a mess ... like I don't know what I'm doing ... which is true)
    // It'd be better and more reliable if I had each media element in the same spot
    // The controls are not the sane style as the YouTube video player controls
    
    for (var galleryIndex = 0; galleryIndex < document.getElementsByClassName('gallery-controls').length; galleryIndex++) {
        //FIXME: I feel there is a better way to write this peice of code
        if (userAgent.includes('android') || userAgent.includes('iphone')) { 
            document.getElementsByClassName('gallery-controls')[galleryIndex].childNodes[0].innerHTML = '1 / ' +
                document.getElementsByClassName('gallery-controls')[galleryIndex].offsetParent.childNodes[1].children.length;
        } else {
            document.getElementsByClassName('gallery-controls')[galleryIndex].childNodes[1].innerHTML = '1 / ' +
                document.getElementsByClassName('gallery-controls')[galleryIndex].offsetParent.childNodes[1].children.length;
        }
    }
    
    if (userAgent.includes('android') || userAgent.includes('iphone')) {
        var originTouch = 0;
        
        for (var postMedia = 0; postMedia < document.getElementsByClassName('post-media').length; postMedia++) {
            document.getElementsByClassName('post-media')[postMedia].addEventListener("touchmove", () => {
                //event.preventDefault();
                
                var postMediaElement = event.target.offsetParent;
                var postMediaIndex = postMediaElement.getAttribute('index');
                var postMediaIndexElement = postMediaElement.previousSibling.childNodes[0];
                
                // event.target
                // event.srcElement
                // event.changedTouches[0].target <--- Exists through all touch types
                // event.targetTouches[0].target
                // event.touches[0].target
                // event.touches[0].target.parentNode
                // event.touches[0].target.parentElement
                // event.touches[0].target.offsetParent <------ THIS ONE!!!
                
                if (originTouch === 0) originTouch = event.changedTouches[0].pageX;
                    
                if ((postMediaElement.clientWidth / 3) < Math.abs(originTouch - event.changedTouches[0].pageX)) {
                    if (postMediaIndex < postMediaElement.childNodes.length -1 && originTouch > event.changedTouches[0].pageX) {
                        postMediaIndex++;
                        postMediaIndexElement.innerHTML = (postMediaIndex + 1) + ' / ' + postMediaElement.childNodes.length;
                    }
                    
                    if (postMediaIndex > 0 && originTouch < event.changedTouches[0].pageX) {
                        postMediaIndex--;
                        postMediaIndexElement.innerHTML = (postMediaIndex + 1) + ' / ' + postMediaElement.childNodes.length;
                    }

                    postMediaElement.setAttribute('index', postMediaIndex);
                    postMediaElement.scrollTo(0, (postMediaElement.style.height.replace('px', '') * postMediaIndex));
                    
                    originTouch = 0;
                }
            });
            
            document.getElementsByClassName('post-media')[postMedia].addEventListener("touchstart", () => {
                originTouch = 0;
            });
        }
    } else {
        for (var galleryControls = 0; galleryControls < document.getElementsByClassName('gallery-controls').length; galleryControls++) {
            document.getElementsByClassName('control-right')[galleryControls].addEventListener('click', () => {
                event = event || window.event;
                
                var postMediaElement = event.path[2].childNodes[1];
                var postMediaIndexElement = event.path[1].childNodes[1], postMediaIndex = postMediaElement.getAttribute('index');
        
                if (postMediaIndex < postMediaElement.childNodes.length -1) {
                    postMediaIndex++;
                    postMediaIndexElement.innerHTML = (postMediaIndex + 1) + ' / ' + postMediaElement.childNodes.length;
                    postMediaElement.setAttribute('index', postMediaIndex);
                    postMediaElement.scrollTo(0, (postMediaElement.style.height.replace('px', '') * postMediaIndex));
                }
            });
            
            document.getElementsByClassName('control-left')[galleryControls].addEventListener('click', () => {
                event = event || window.event;

                var postMediaElement = event.path[2].childNodes[1];
                var postMediaIndexElement = event.path[1].childNodes[1], postMediaIndex = postMediaElement.getAttribute('index');

                if (postMediaIndex > 0) {
                    postMediaIndex--;
                    postMediaIndexElement.innerHTML = (postMediaIndex + 1) + ' / ' + postMediaElement.childNodes.length;
                    postMediaElement.setAttribute('index', postMediaIndex);
                    postMediaElement.scrollTo(0, (postMediaElement.style.height.replace('px', '') * postMediaIndex));
                }
            });
        }
    }
}

/**
 * Grabs data entries by date from the datastore. These are then displayed to the user.
 */
function grabContent() {
    //Sets the destination of the POST request
    var json = {};
    json.type = "datastore";
    json.mode = "pull";
    json.entity = entity;
    
    var keySet = [];
    var index = -1;
        
    //Checks if the list is larger than 5, makes sure that index is a patameter in the query string, and then splits the keys into sets of 5
    if (list.length > 5) {
        if (queryString.includes('i=')) {
            for (var i in queryParameters) {
                if (queryParameters[i].startsWith('i=')) {
                    index = queryParameters[i].replace( /^\D+/g, '');
                    //json.page = index;
                }
            }
        } else {
            index = 0;
        }
        
        while (list.length > 0) {
            console.log('Why do we have this in a loop');
            keySet.push(list.splice(0, 5));
        }
    } else if (list.length > 0) {
        keySet = [list];
        index = 0;
    }
    
    var theChoosenOnes = keySet[index];
    json.key = [];
    
    for (var i = 0; i < theChoosenOnes.length; i++) {
        json.key.push(theChoosenOnes[i].key);
    }
    
    $.post("/", JSON.stringify(json))
        .done((data) => {
            document.getElementById("content").innerHTML += entryBuilder(data, keySet.length);
            mediaGalleryControls();
        })
        .fail((error) => {
            var postEntry = {};
            postEntry.title = 'Retrieval Error';
            postEntry.media = ['/img/meme/shrug.png'];
            postEntry.doc = 'I have absolutely nothing to show case, as something on the backend just kinda ... broke.';

            document.getElementById("content").innerHTML += entryBuilder(data);

            console.error(error.status);
        });
}