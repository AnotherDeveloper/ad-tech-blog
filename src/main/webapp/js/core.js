var navList = '<li id="nav-softDev"><a href="/df?p=software">Software Development</a></li><li id="nav-elecEng"><a href="/df?p=electrical">Hardware Design</a></li><li id="nav-animation"><a href="/df?p=animation">Animations</a></li><li id="nav-about"><a href="/df?p=about">About</a></li>';
var navHTML = '<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"><div class="container"><div class="navbar-header"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><div id="nav-home"><a href="/df?p=home"><img class="navbar-brand" src="img/full_logo.png" style="padding: 5px;"></a></div></div><div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"><ul id="navList" class="nav navbar-nav navbar-left">' + navList + '</ul><div class="navbar-search navbar-right" id="search" ><input class="navbar-search" placeholder="Search"><img class="navbar-search" src="/img/search.png"></div></div></div></nav>';
var footerHTML = '<hr><a href="https://www.linkedin.com/in/jasper-creyf-840a38114"><img class="icon" src="./img/linkedin.png"></a><a href="https://github.com/Another-Developer"><img class="icon" src="./img/git.png"></a><!--<a href="https://bitbucket.org/AnotherDeveloper/"><img class="icon" src="./img/bitbucket.png"></a>--><a href="http://stackexchange.com/users/1535737/jasper-creyf?tab=accounts"><img class="icon" src="./img/stackex.png"></a><a href="https://www.youtube.com/c/Spacebirdy/"><img class="icon" src="./img/youtube.png"></a><br><small><a href="/df?p=privacy">Privacy Policy</a> | <a href="javascript:personalPreferences()">Personal Preferences</a></small>';
var head = document.getElementsByTagName('head')[0];

var userAgent = navigator.userAgent.toLocaleLowerCase();

//An event listener for when the user uses their history backward or forward buttons
window.addEventListener('popstate', (event) => {
    grabPage();
});

/*
 * When a GET request is made, the df page is loaded and initDefaults gets fired.
 * The function injects a few tags with HTML code, grabs the desired page, and applies
 * a listener to for the search bar
 */
function initDefaults() {
    document.getElementById('navigation-bar').innerHTML = navHTML;
    document.getElementsByTagName('footer')[0].innerHTML = footerHTML;
    
    var searchDiv = document.getElementById('search');
    var searchInput = document.getElementsByClassName('navbar-search')[1];

    //Makes the search input field visible
    searchDiv.addEventListener('mouseover', () => {
        searchInput.style.opacity = '1';
        searchInput.style.width = '100%';
        searchDiv.style.width = '100%';
    });
    
    //Hides the  search input field
    searchDiv.addEventListener('mouseout', () => {
        if (searchInput.value.length === 0) {
            searchInput.style.opacity = '';
            searchInput.style.width = '';
            searchDiv.style.width = '';
        }
    });
    
    searchInput.addEventListener('keypress', (evt) => {
        if (evt.keyCode === 13 && searchInput.value.length > 0) search(searchInput.value);
    });
    
    document.getElementsByClassName('navbar-search')[2].addEventListener('click', () => {
        if (searchInput.value.length > 0) search(searchInput.value);
    });
    
    $('a').click((evt) => {
        evt.preventDefault();

        if(!evt.currentTarget.baseURI.includes(evt.currentTarget.hostname)) 
            window.location.href = evt.currentTarget.href;

        grabPage(evt.currentTarget.href);
    });
    
    grabPage();
}

function search(searchContents) {
    alert('This feature will be fully implemented on a later date.'); //TODO: Implement search feature
}

/*
 * Sets the selected page its element in the navagation bar to 'active'
 * 
 * @param {String} selectedPage
 */
function setActive(selectedPage) {
    document.getElementById('navList').innerHTML = navList;
    
    var doc = document.getElementById(selectedPage);
    doc.className = 'active';
}

/*
 * The function grabPage() grabs the URL within the address-bar or the supplied onr
 * and sends a JSON POST request with the URL to the server. The server either returns 
 * the requested data, or an error code. If there is an error code, the error is 
 * handled and displayed.
 * 
 * @param {URL} url The URL to the page on the server that needs to be displayed
 */
function grabPage(url) {
// The request for scripts to be unloaded needs to happen here
    
    var json = {};
    json.type = "page";
    
    if (typeof url !== 'undefined') {
        window.history.pushState(null, null, url);
        json.page = document.location.href;
    } else {
        json.page = document.location.href;
    }
    
    $.post('/', JSON.stringify(json))
        .done((data) => {
            //The returned data is split. The first entry defines the variables for the <head>, while the second is for the <body>
            var response = data.split('<!-- /.end -->');
            var pageMeta = JSON.parse(response[0]);
            
            //The processed data is appended
            document.title = pageMeta.title;
            if (typeof pageMeta.script !== 'undefined') appendScripts(pageMeta.script);
            
            document.getElementById('page-content').innerHTML = response[1];
            document.getElementsByTagName('footer')[0].innerHTML = footerHTML;
        })
        .fail((error) => {
            window.history.pushState(null, null, 'df?p=error');
            json.page = document.location.href;
    
            $.post('/', JSON.stringify(json))
            .done((data) => {
                //Adds a replaceAll function to replace all matching pieces
                String.prototype.replaceAll = function(search, replacement) {
                    var target = this;
                    return target.replace(new RegExp(search, 'g'), replacement);
                };
                
                var html;
                html = data.replaceAll('<!-- /.error-code -->', error.status);
                
                //A switch statement is used to link to it's error code, then the error page is modified to meet the meanings of the error
                switch(error.status) { //It makes no sense that I'm required to set the variable equal to itself, before I can just replace anything
                    case 400:
                        html = html.replace('<!-- /.error-type -->', "Bad Request");
                        html = html.replace('<!-- /.error-message -->', "Huh, a bad request. What's that?");
                        break;
                    case 401:
                        html = html.replace('<!-- /.error-type -->', "Unauthorized");
                        html = html.replace('<!-- /.error-message -->', "Please authenticate yourself and try again.");
                        break;
                    case 403:
                        html = html.replace('<!-- /.error-type -->', "Forbidden");
                        html = html.replace('<!-- /.error-message -->', "Hey! That stuff is private ... and you're not on the list.");
                        break;
                    case 404:
                        html = html.replace('<!-- /.error-type -->', "Page Not Found");
                        html = html.replace('<!-- /.error-message -->', "My cat stole my coffee mug ._.");
                        html = html.replace('id="error-background"', "style=\"background-repeat: no-repeat; background-image: url(/img/meme/cat_in_da_mug.gif); background-size: 40%; background-position: right;\"");
                        break;
                    case 500:
                        html = html.replace('<!-- /.error-type -->', "Internal Server Error");
                        html = html.replace('<!-- /.error-message -->', "Well ... something went wrong.");
                        break;
                    case 503:
                        html = html.replace('<!-- /.error-type -->', "Service Unavailable");
                        html = html.replace('<!-- /.error-message -->', "I don't know, don't look at me.");
                        break;
                    default:
                        html = html.replace('<!-- /.error-type -->', "Unknown Error");
                        html = html.replace('<!-- /.error-message -->', "This error hasn't been documented in my code.");
                        break;
                }
                
                var response = html.split('<!-- /.end -->');
                var pageMeta = JSON.parse(response[0]);

                document.title = pageMeta.title;
                if (typeof pageMeta.script !== 'undefined') appendScripts(pageMeta.script);

                document.getElementById('page-content').innerHTML = response[1];
                document.getElementsByTagName('footer')[0].innerHTML = footerHTML;
            });
        });
}

/*
 * Builds a <script> tag from a URI and appends it to the <head> tag.
 * 
 * @param filePath The URI to the script that needs to be loaded
 */
function appendScripts(filePath) {
    var scriptArray = filePath;
    
    //FIXME: Don't get the script again if we've already got it
    
    for (i = 0; i < scriptArray.length; i++) {
        var script = document.createElement('script');
        script.setAttribute('type','text/javascript');
        script.setAttribute('src', scriptArray[i]);
        script.setAttribute('id', 'removable');
        head.appendChild(script);
    }
}

/*
 * Stores a user's contact request in the datastore ... so that I can contact them back :D
 */
function contact() {
    //Setting the destination POST data
    var json = {};
    json.type = "authentication";
    json.name = document.getElementById('name').value;
    json.email = document.getElementById('email').value;
    json.message = document.getElementById('message').value;
    json.response = grecaptcha.getResponse();

    //Check if the reCaptcha response is not blank, and then set the image with-in the submit button
    if (!(json.response === "")) {
        document.getElementById('response').innerHTML = "";
        
        document.getElementById('submit').disabled = true;
        document.getElementById('submit').src = "./img/submiting.gif";
        
        $.post("/", JSON.stringify(json))
        //Data to server was submitted successfully. The submit button image is changed in accordance. Then a message is displayed.
        .done((data) => {
            document.getElementById('submit').src = "./img/done.png";
            var responseJSON = JSON.parse(data);
    
            if (responseJSON['success'] === true) {
                document.getElementById('response').innerHTML = "Your message has been sent.";
            } else if (responseJSON['success'] === false) {
                document.getElementById('response').innerHTML = "Oh no! I think you might be a robot ... are you?";
            }
        })
        //Well, I got some bad news for ya, the data we just created couldn't be processed by the server.
        .fail(() => {
            document.getElementById('response').innerHTML = "Error 500. An internal server error has occured. Can't handle request.";
        });
    } else {
        document.getElementById('response').innerHTML = "Please verify that you're not a robot.";
    }
}

function personalPreferences() {
    alert('This feature will be fully implemented on a later date.'); //TODO: Implement this feature. Guidelines are on the home page.
}