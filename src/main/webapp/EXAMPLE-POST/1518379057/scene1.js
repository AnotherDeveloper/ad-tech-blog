const postID = "1518379057scene1";
const contentPath = "EXAMPLE-POST/1518379057/";
const preLoadScripts = [
    "js/three-js/three.js", //Update to minimum version 
    "js/three-js/loaders/MTLLoader.js",
    "js/three-js/loaders/OBJLoader.js",
    "js/three-js/Detector.js"
];

// Can't have container as global refrence, or I must find a way to have each scene script in a different scope
// Will thread's cause a problem of having no graphics? 
var container = document.getElementById(postID);
var containerWidth = 0, containerHeight = 0;

var canvas;

var scene, renderer, camera, object, origin, skySphere;

/*
 * TODO: Document this functions usage
 */
function preInit() {
    container.style.backgroundColor = '#101010';
    container.style.backgroundImage = 'url(/' + contentPath + 'scene1Cover.png)';
    container.style.backgroundSize = 'cover';
    container.style.backgroundPosition = 'center';
    
    var startButton = document.createElement('div');
    startButton.style.height = 'inherit';
    startButton.style.backgroundImage = 'url(/img/start.png)';
    startButton.style.backgroundPosition = 'center';
    startButton.style.backgroundRepeat = 'no-repeat';
    container.appendChild(startButton);
    
    var exitFsButton, fullscreenType = -1;
    
    switch (window.navigator.platform) {
        case 'iPhone':
        case 'iPod':
        case 'iPad':
        case 'iPhone Simulator':
        case 'iPod Simulator':
        case 'iPad Simulator':
            fullscreenType = 0;
            container.addEventListener('click', enterFullscreen);
            
            break;
        default:
            if (userAgent.includes('android')) {
                fullscreenType = 1;
                container.addEventListener('click', enterFullscreen);
            } else {
                container.addEventListener('click', loadScene);
            }
            
            break;
    }
    
    /*
     * TODO: Document this functions usage
     */
    function enterFullscreen() {    
        container.style = null;
        container.style.backgroundColor = '#101010';
        container.removeEventListener('click', enterFullscreen);
        
        exitFsButton = document.createElement('div');
        exitFsButton.style.backgroundImage = 'url(/img/exit.png)';
        exitFsButton.style.backgroundPosition = 'center';
        exitFsButton.style.backgroundRepeat = 'no-repeat';
        exitFsButton.style.backgroundSize = 'cover';
        exitFsButton.style.position = 'fixed';
        exitFsButton.style.top = '5px';
        exitFsButton.style.right = '5px';
        
        if (window.innerHeight > window.innerWidth) {
            exitFsButton.style.width = (window.innerHeight * 0.025) + 'px';
            exitFsButton.style.height = (window.innerHeight * 0.025) + 'px';
            exitFsButton.style.margin = (window.innerHeight * 0.0125) + 'px';
        } else {
            exitFsButton.style.width = (window.innerWidth * 0.025) + 'px';
            exitFsButton.style.height = (window.innerWidth * 0.025) + 'px';
            exitFsButton.style.margin = (window.innerWidth * 0.0125) + 'px';
        }
        
        window.addEventListener('orientationchange', () => {            
            if (window.innerHeight > window.innerWidth) {
                exitFsButton.style.width = (window.innerHeight * 0.025) + 'px';
                exitFsButton.style.height = (window.innerHeight * 0.025) + 'px';
                exitFsButton.style.margin = (window.innerHeight * 0.0125) + 'px';
            } else {
                exitFsButton.style.width = (window.innerWidth * 0.025) + 'px';
                exitFsButton.style.height = (window.innerWidth * 0.025) + 'px';
                exitFsButton.style.margin = (window.innerWidth * 0.0125) + 'px';
            }
        });
        
        switch (fullscreenType) {
            case 0: // Pseudo fullscreen (iPhone)
                container.style.top = 0;
                container.style.left = 0;
                container.style.width = screen.width + 'px';
                container.style.height = screen.height + 'px';
                container.style.position = 'fixed';
                container.style.zIndex = 10000;
                
                window.scrollTo(0, 0);
                
                // TODO: Pseudo fullscreen for iOS
                // Set container to screen dimensions
                // Scroll 1 pixel down
                // Adjust container dimensions to window inner dimensions
                // 
                // window.scrollTo(0, screen.height - window.innerHeight);
                // https://gist.github.com/scottjehl/1183357
                // scrollY
                // screen.height
                // document.body.offsetHeight
                // window.innerHeight
                
                // Do / show a swipe up to enter fullscreen
                // p that reacts to how far you've scrolled
                
                window.addEventListener('orientationchange', () => {            
                    container.style.width = screen.width + 'px';
                    container.style.height = screen.height + 'px';
                    
                    window.scrollTo(0, 0);
                });
                
                exitFsButton.addEventListener('click', () => {
                    container.style = null;
                    onWindowResize();
                    
                    unloadScene();
                });
                
                loadScene();
                
                break;
            case 1: // True fullscreen (Android)
                if      (container.mozRequestFullScreen) {     container.mozRequestFullScreen(); }
                else if (container.webkitRequestFullscreen) {  container.webkitRequestFullscreen(); }
                
                setTimeout(() => {
                    if (document.webkitIsFullScreen === true || document.mozFullScreen === true) {
                        loadScene();
                    } else {
                        //TODO: Log error and handle it by going pseudo fullscreen (specifically for Android)
                        
                        console.error('Failed to enter fullscreen, using failsafe');
                    }
                }, 500);
                
                exitFsButton.addEventListener('click', () => {
                    if      (document.mozCancelFullScreen)   { document.mozCancelFullScreen(); }
                    else if (document.webkitExitFullscreen)  { document.webkitExitFullscreen(); }
                    
                    unloadScene();
                });
                
                break;
        }
    }
    
    /*
     * Displays the controls that are available to the user
     * This is shown before the scene gets loaded
     */
    function showControls() {
        
    }
    
    function unloadScene() {
        container.innerHTML = null;
        
        container.removeEventListener('mousewheel', onMouseWheel, false);
        container.removeEventListener('DOMMouseScroll', onMouseWheel, false); // <- FireFox wants to be special
        container.removeEventListener('mousedown', onMouseDown, false);
        container.removeEventListener('mousemove', onMouseMove, false);
        container.removeEventListener('mouseup', onMouseUp, false);

        container.removeEventListener('touchstart', onTouchStart, false);
        container.removeEventListener('touchmove', onTouchMove, false);
        container.removeEventListener('touchend', onTouchEnd, false);
        
        setTimeout(preInit, 500);   
    }
    
    /*
     * TODO: Document this functions usage
     */
    function loadScene() {
        container.removeEventListener('click', loadScene);
        
        var loadedScripts = 0;
        
        for (var s in preLoadScripts) {
            var scriptURI = preLoadScripts[s];

            jQuery.ajax({
                url: scriptURI,
                dataType: 'script',
                complete: () => {
                    loadedScripts++;

                    if (loadedScripts === preLoadScripts.length ) {
                        container.innerHTML = null;
                        if (fullscreenType > -1) container.appendChild(exitFsButton);
                        
                        init();
                    }
                },
                error: (jQxhr, errorMessage, errorThrown) => {
                    console.error(scriptURI);
                    //console.error(jQxhr, errorMessage, errorThrown);
                },
                async: false
                // What do I need to do to keep this asyncronous?
                // As it seems callbacks and deffered objects aren't working
            });
        }
    }
}

preInit();

/*
 * TODO: Document this functions usage
 */
function init() {
    //TODO: Remove the point light from the camera and put it in front of the sun and enable shadows
    
    containerWidth = container.offsetWidth;
    containerHeight = container.offsetHeight;
    
    camera = new THREE.PerspectiveCamera(45, containerWidth / containerHeight, 0.5, 500);
    camera.position.x = 5;
    camera.lookAt(0, 0, 0);
    
    origin = new THREE.Group();
    origin.rotation.z = 0.3;
    origin.add(camera);

    scene = new THREE.Scene();
    clock = new THREE.Clock();

    var ambientLight = new THREE.AmbientLight(0xffffe0, 0.8);
    scene.add(ambientLight);
    
    var pointLight = new THREE.PointLight(0xffffff, 0.25);
    camera.add(pointLight);
    scene.add(origin);

    var skyGeo = new THREE.SphereGeometry(100, 25, 25);
    var texLoader = new THREE.TextureLoader(), skyTexture = texLoader.load(contentPath + 'Sky.png');
    var skyMaterial = new THREE.MeshPhongMaterial({ 
        map: skyTexture
    });
    
    skySphere = new THREE.Mesh(skyGeo, skyMaterial);
    skySphere.material.side = THREE.BackSide;
    scene.add(skySphere);

    var onProgress = function (xhr) {};
    var onError = function (xhr) {};

    var mtlLoader = new THREE.MTLLoader();
    mtlLoader.setPath(contentPath);
    mtlLoader.load('scene1.mtl', function (materials) {
        materials.preload();

        var objLoader = new THREE.OBJLoader();
        objLoader.setMaterials(materials);
        objLoader.setPath(contentPath);
        objLoader.load('scene1.obj', function (mesh) {
            object = mesh;
            object.position.y = -0.5;
            
            scene.add(object);
        }, onProgress, onError);

    });

    renderer = new THREE.WebGLRenderer();
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(containerWidth, containerHeight);
    container.appendChild(renderer.domElement);
    
    window.addEventListener('resize', onWindowResize, false);
    
    container.addEventListener('mousewheel', onMouseWheel, false);
    container.addEventListener('DOMMouseScroll', onMouseWheel, false); // <- FireFox wants to be special
    container.addEventListener('mousedown', onMouseDown, false);
    container.addEventListener('mousemove', onMouseMove, false);
    container.addEventListener('mouseup', onMouseUp, false);
    
    container.addEventListener('touchstart', onTouchStart, false);
    container.addEventListener('touchmove', onTouchMove, false);
    container.addEventListener('touchend', onTouchEnd, false);
    
    animate();
    
    canvas = container.children[0];
}

/*
 * Prevents the default browser action of what ever type of event is being fired.
 */
function preventDefault(event) {
    event = event || window.event;

    if (event.preventDefault) event.preventDefault();

    event.returnValue = false;  
}

// Left: 37, Up: 38, Right: 39, Down: 40
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

/*
 * Takes all keys from the key object that need their default behavior disabled.
 * 
 * Example: Arrow keys will scroll the page, but you'd probably want it only
 *          moving something in your scene.
 */
function preventDefaultOnKeys(event) {
    if (keys[event.keyCode]) {
        preventDefault(event);
        
        return false;
    }
}

/*
 * Disabled page scrolling
 */
function disableScroll() {
    if (window.addEventListener) window.addEventListener('DOMMouseScroll', preventDefault, false);

    window.onwheel      = preventDefault;
    window.onmousewheel = document.onmousewheel = preventDefault;
    window.ontouchmove  = preventDefault;
}

/*
 * Enables page scrolling
 */
function enableScroll() {
    if (window.removeEventListener) window.removeEventListener('DOMMouseScroll', preventDefault, false);
    
    window.onmousewheel = document.onmousewheel = null; 
    window.onwheel      = null;
    window.ontouchmove  = null;
}

/*
 * Upon the window getting resized, the container width and height variable values are updated, 
 * as well as the aspect ratio and size of the canvas.
 */
function onWindowResize() { // FIXME: Stays behind as a residule listener
    containerWidth = container.offsetWidth;
    containerHeight = container.offsetHeight;
    
    camera.aspect = containerWidth / containerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(containerWidth, containerHeight);
}

/*
 * An event listener function that lets the user zoom in and out by setting the global 
 * zoomDelta variable, which the variable is handled by the animate() function
 */
function onMouseWheel(event) {
    event = event || window.event;
    
    disableScroll();
    
    if ((event.detail !== 0 && event.detail > 0) || event.deltaY > 0) {
        zoomDelta = zoomDelta + 0.125;
    } else {
        zoomDelta = zoomDelta - 0.125;
    }
    
    setTimeout(() => {
        enableScroll();
        zoomDelta = 0;
    }, 100);
}

var isDraggingInput = false, inputDeltaX = 0, inputDeltaY = 0;
var userTookControl = false;

/*
 * This stops the object's default rotating action, sets the isDraggingInput boolean 
 * variable to identify the mouse as being dragged, and locks the mouse to the canvas.
 */
function onMouseDown() {
    isDraggingInput = true;
    userTookControl = true;
    
    canvas.requestPointerLock = 
            canvas.requestPointerLock || canvas.mozRequestPointerLock || canvas.webkitRequestPointerLock;
    canvas.requestPointerLock();
    
    container.style.cursor = 'none';
}

/*
 * Set the delta of what the mouse has done since last sample
 */
function onMouseMove(event) {
    if (isDraggingInput) {
        inputDeltaX = event.movementX / 80;
        inputDeltaY = event.movementY / 80;
    }
}

/*
 * This unsets the isDraggingInput boolean variable to identify the mouse as no longer 
 * being dragged, and releases the mouse lock.
 */
function onMouseUp() {
    isDraggingInput = false;
    
    document.exitPointerLock = 
            document.exitPointerLock || document.mozExitPointerLock || document.webkitExitPointerLock;
    document.exitPointerLock();
    
    container.style.cursor = 'default';
}

var previousTouchX, previousTouchX;
var differenceX, differenceY, zoomStart, zoomCurrent, zoomDelta = 0;

/*
 * TODO: Document this functions usage
 */
function onTouchStart(event) {
    event = event || window.event;
    
    switch (event.touches.length) {
        case 1:
            isDraggingInput = true;
            userTookControl = true;
            
            previousTouchX = event.touches[0].pageX;
            previousTouchY = event.touches[0].pageY;
            break;
        case 2:
            differenceX = Math.abs(event.touches[0].pageX - event.touches[1].pageX);
            differenceY = Math.abs(event.touches[0].pageY - event.touches[1].pageY);
            
            zoomStart = Math.round(Math.sqrt(differenceX * differenceX + differenceY * differenceY));
            break;
    }
}

/*
 * TODO: Document this functions usage
 */
function onTouchMove(event) {
    event = event || window.event;
    event.stopPropagation();
    event.preventDefault();
    
    switch (event.touches.length) {
        case 1:
            inputDeltaX = (event.touches[0].pageX - previousTouchX) / 160; 
            inputDeltaY = (event.touches[0].pageY - previousTouchY) / 160; 
            
            previousTouchX = event.touches[0].pageX;
            previousTouchY = event.touches[0].pageY;
            break;
        case 2:
            differenceX = Math.abs(event.touches[0].pageX - event.touches[1].pageX);
            differenceY = Math.abs(event.touches[0].pageY - event.touches[1].pageY);
            
            zoomCurrent = Math.round(Math.sqrt(differenceX * differenceX + differenceY * differenceY));
            zoomDelta = (zoomCurrent - zoomStart) / 320;
            
            break;
    }
}

/*
 * TODO: Document this functions usage
 */
function onTouchEnd(event) {
    event = event || window.event;
    
    isDraggingInput = false;
    
    switch (event.touches.length) {
        default : //When single touch ends
            
            break;
        case 1: // When double touch ends
            zoomDelta = 0;
            break;
        case 2: // When triple touch ends
            console.log("Wasn't expecting this! Nor was I expecting that I'd be off by one either :/");
            break;
    }
}

/*
 * This function updates the camera position around the object(s) while panning or zooming
 * and then calls for the frame to get rendered.
 */
function animate() {
    if (isDraggingInput) {
        // Adjusts the rotation of the camera's origin (located at 0, 0, 0)
        origin.rotation.y = origin.rotation.y - inputDeltaX;
        origin.rotation.z = origin.rotation.z + inputDeltaY;
        
//        if (inputDeltaY > 0) { console.log("The camera goes higher on the Y axis"); }
//        if (inputDeltaY < 0) { console.log("The camera goes lower on the Y axis"); }
//        if (inputDeltaX > 0) { console.log("The camera pans around the object in the left direction"); }
//        if (inputDeltaX < 0) { console.log("The camera pans around the object in the right direction"); }
//        Circles http://www.math.com/tables/geometry/circles.htm
//        Spheres http://mathworld.wolfram.com/Sphere.html
        
        inputDeltaX = 0, inputDeltaY = 0;
    }
    
    // This if statement for zooming was made before the one for panning ... 
    // which the one for panning was done the easy way.
    if (zoomDelta !== 0 && typeof zoomDelta !== 'undefined') {
        var currentDFC, newDFC; // DFC is Distance From Center.
        
        // Calculate the distance from center using the "pythirdean theorem"?
        currentDFC = Math.sqrt(
                Math.abs(camera.position.x * camera.position.x) +
                Math.abs(camera.position.y * camera.position.y) +
                Math.abs(camera.position.z * camera.position.z));
        
        // Sets the new distance from center
        newDFC = currentDFC + zoomDelta;
        
        // Uses a ratio of (new over old) to scale all cordinates
        if ((currentDFC * (newDFC / currentDFC)) > 0.9 && (currentDFC * (newDFC / currentDFC)) < 26) {
            camera.position.x = camera.position.x * (newDFC / currentDFC);
            camera.position.y = camera.position.y * (newDFC / currentDFC);
            camera.position.z = camera.position.z * (newDFC / currentDFC);
        }
        
        // Could just set the camera's X position and call it a day, but I want to come back to
        // this to do the panning the more complicated way too.
        // TODO: Do camera panning the fun way
    }
    
    var delta = clock.getDelta();
    
    // Rotates the camera's origin until interupted
    if (origin !== undefined && !userTookControl) {
        origin.rotation.y += delta * 0.15;
    }
    
    requestAnimationFrame(animate);
    render();
}

/*
 * Called on to render a new frame.
 */
function render() {
    renderer.render(scene, camera);
}
